package com.nimo.readwritesplitting.entity;

import lombok.Getter;
import lombok.Setter;

/**
 * @auther zgp
 * @desc
 * @date 2021/3/14
 */
@Getter
@Setter
public class UserInfo {

    private Long id;

    private String username;

    private String password;

    private Long goodsId;

}

