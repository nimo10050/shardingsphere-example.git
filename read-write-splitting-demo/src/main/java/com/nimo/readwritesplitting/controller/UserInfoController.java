package com.nimo.readwritesplitting.controller;

import com.nimo.readwritesplitting.entity.UserInfo;
import com.nimo.readwritesplitting.mapper.UserInfoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @auther zgp
 * @desc
 * @date 2021/4/28
 */
@RestController
public class UserInfoController {

    @Autowired
    private UserInfoMapper userInfoMapper;

    @RequestMapping("userinfo/{id}")
    public Object getUserInfo(@PathVariable Long id) {
        return userInfoMapper.getUser(id);
    }

    @PostMapping("userinfo")
    public Object addUserInfo(@RequestBody UserInfo userInfo) {
        return userInfoMapper.addUser(userInfo);
    }

}
