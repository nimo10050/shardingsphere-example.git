package com.nimo.readwritesplitting;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.nimo.readwritesplitting.mapper")
public class ReadWriteSplittingDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(ReadWriteSplittingDemoApplication.class, args);
    }

}
