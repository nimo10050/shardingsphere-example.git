package com.nimo.readwritesplitting.mapper;


import com.nimo.readwritesplitting.entity.UserInfo;

/**
 * @auther zgp
 * @desc
 * @date 2021/3/14
 */
public interface UserInfoMapper {

    UserInfo getUser(Long id);

    int addUser(UserInfo userInfo);
}
