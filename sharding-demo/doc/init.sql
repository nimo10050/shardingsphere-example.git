create database miaosha;

DROP TABLE IF EXISTS `user_info0`;
CREATE TABLE `user_info0`
(
    `id`         bigint(20)                    NOT NULL AUTO_INCREMENT,
    `user_label` varchar(32) COLLATE utf8_bin           DEFAULT NULL,
    `username`   varchar(64) COLLATE utf8_bin           DEFAULT NULL,
    `email`      varchar(64) COLLATE utf8_bin           DEFAULT NULL,
    `phone`      varchar(64) COLLATE utf8_bin           DEFAULT NULL,
    `password`   varchar(128) COLLATE utf8_bin NOT NULL,
    `active`     tinyint(4)                    NOT NULL DEFAULT '1',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 7
  DEFAULT CHARSET = utf8
  COLLATE = utf8_bin;


DROP TABLE IF EXISTS `user_info1`;
CREATE TABLE `user_info1`
(
    `id`         bigint(20)                    NOT NULL AUTO_INCREMENT,
    `user_label` varchar(32) COLLATE utf8_bin           DEFAULT NULL,
    `username`   varchar(64) COLLATE utf8_bin           DEFAULT NULL,
    `email`      varchar(64) COLLATE utf8_bin           DEFAULT NULL,
    `phone`      varchar(64) COLLATE utf8_bin           DEFAULT NULL,
    `password`   varchar(128) COLLATE utf8_bin NOT NULL,
    `active`     tinyint(4)                    NOT NULL DEFAULT '1',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 6
  DEFAULT CHARSET = utf8
  COLLATE = utf8_bin;