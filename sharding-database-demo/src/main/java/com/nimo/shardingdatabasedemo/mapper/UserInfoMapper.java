package com.nimo.shardingdatabasedemo.mapper;


import com.nimo.shardingdatabasedemo.entity.UserInfo;

/**
 * @auther zgp
 * @desc
 * @date 2021/3/14
 */
public interface UserInfoMapper {

    UserInfo getUser(Long id);

    int addUser(UserInfo userInfo);
}
